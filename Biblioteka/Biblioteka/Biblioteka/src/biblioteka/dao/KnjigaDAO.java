package biblioteka.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import biblioteka.model.Autor;
import biblioteka.model.Knjiga;

import util.ConnectionManager;

public class KnjigaDAO {
	
	public static List<Knjiga> getAll(){
		
		String query = "select k.id, k.naslov, a.id, a.ime, a.prezime from knjiga k join autor a"
				+ " on k.id_autora = a.id";
		
		Statement stmt;
		List<Knjiga> retVal = null;
		try {
			stmt = ConnectionManager.getConnection().createStatement();
			ResultSet rset = stmt.executeQuery(query);
			retVal = new ArrayList<Knjiga>();
			while (rset.next()) {
				int id = rset.getInt(1);
				String naslov = rset.getString(2);
				
				int idAutora = rset.getInt(3);
				String ime = rset.getString(4);
				String prezime = rset.getString(5);
				Autor a = new Autor(idAutora, ime, prezime);
				Knjiga k = new Knjiga(id, naslov, a);
				retVal.add(k);
			}
			rset.close();
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal;
	}

	

    public static boolean insert(String naslov, int autor_id){
		
		boolean retVal = false;
		try {
			String update = "INSERT INTO Knjiga (naslov, id_autora) values (?, ?)";
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, naslov);
			pstmt.setInt(2, autor_id);
			if (pstmt.executeUpdate() == 1) {
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
		
	}
	
     public static List<Knjiga> getByAutor(int idAutora){
		
		String query = "SELECT * FROM Knjiga JOIN Autor ON id = id_autora WHERE id_autora= ? ";
		
		
		List<Knjiga> retVal = null;
		try {
			Connection conn = ConnectionManager.getConnection();
			PreparedStatement preparedStatement = conn.prepareStatement(query);
		    preparedStatement.setInt(3, idAutora);
			ResultSet rset = preparedStatement.executeQuery(query);
			retVal = new ArrayList<Knjiga>();
			while (rset.next()) {
				int id = rset.getInt(1);
				String naslov = rset.getString(2);
				
				 idAutora = rset.getInt(3);
				String ime = rset.getString(4);
				String prezime = rset.getString(5);
				Autor a = new Autor(idAutora, ime, prezime);
				Knjiga k = new Knjiga(id, naslov, a);
				retVal.add(k);
			}
			rset.close();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal;
	}

     public static List<Knjiga> getByNaslov(String naslov){
 		
 		String query = "SELECT * FROM Knjiga WHERE naslov = ?";
 		
 		
 		List<Knjiga> retVal = null;
 		try {
 			Connection conn = ConnectionManager.getConnection();
 			PreparedStatement preparedStatement = conn.prepareStatement(query);
 		    preparedStatement.setString(2, naslov);
 			ResultSet rset = preparedStatement.executeQuery(query);
 			retVal = new ArrayList<Knjiga>();
 			while (rset.next()) {
 				int id = rset.getInt(1);
 	            int idAutora = rset.getInt(3);
 				String ime = rset.getString(4);
 				String prezime = rset.getString(5);
 				Autor a = new Autor(idAutora, ime, prezime);
 				Knjiga k = new Knjiga(id, naslov, a);
 				retVal.add(k);
 			}
 			rset.close();
 			preparedStatement.close();
 		} catch (SQLException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
 		return retVal;
 	}
}


