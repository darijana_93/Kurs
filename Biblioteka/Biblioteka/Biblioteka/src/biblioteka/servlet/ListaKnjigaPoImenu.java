package biblioteka.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import biblioteka.dao.KnjigaDAO;
import biblioteka.model.Knjiga;
;

/**
 * Servlet implementation class ListaKnjigaPoImenu
 */
public class ListaKnjigaPoImenu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListaKnjigaPoImenu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String naslov=request.getParameter("naslov");
		List<Knjiga> knjigePoNaslovu = KnjigaDAO.getByNaslov(naslov);
	response.setContentType("text/html");
		
	PrintWriter pout = response.getWriter();
		pout.println("<html>");
	pout.println("<head>");
		pout.println("</head>");
	pout.println("<body>");
	pout.println("Knjige po naslovu:");
	pout.println("<form action=\"ListaKnjigaPoImenu\" method=\"get\"><input type=\"text\" name=\"naslov\"/></form>");
		pout.println("<table border=\"1\"><tr bgcolor=\"lightgrey\"><th>Knjiga</th><th>Autor</th></tr>");
		for ( Knjiga p : knjigePoNaslovu ) {
		pout.println("<tr>");

			pout.println("<td>" + p.getNaslov() + "</td>");
			pout.println("<td>" + p.getAutor().getIme() + " " + p.getAutor().getPrezime() + "</td>");
		pout.println("</tr>");
	}
		pout.println("</table>");


		pout.println("</body>");
	pout.println("</html>");
		
		}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
