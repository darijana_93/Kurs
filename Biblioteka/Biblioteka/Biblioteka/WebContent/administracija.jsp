<%@ page import = "biblioteka.model.Autor"%>
<%@ page import = "biblioteka.model.Knjiga"%>
<%@ page import = "java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Administracija</title>
<script src="jquery-1.11.0.js">
function provera(){
	ime = document.loginForma.ime.value;
	prezime = document.loginForma.prezime.value;
	
	poruka = '';
	if (ime == ''){
		poruka += 'Niste uneli ime\n';
		document.getElementById('imeSpan').style.visibility = 'visible';
		document.getElementById('imeSpan').style.color = 'green';
	}
	else
		document.getElementById('imeSpan').style.visibility = 'hidden';
	if (prezime == '' ){ 
		poruka += 'Niste uneli prezime';
		document.getElementById('prezimeSpan').style.visibility = 'visible';
	}
	else
		document.getElementById('prezimeSpan').style.visibility = 'hidden';
		
	if (poruka != ''){
		alert(poruka);
		return false;
	}
		
	return true;
	
  }

	
	function usao(spanId){
		document.getElementById(spanId).style.visibility = 'hidden';
	}
	

	
	function izasao(element, spanId){
		value = element.value;
		if (value == '')
			document.getElementById(spanId).style.visibility = 'visible';
	}
	$(document).ready(function(){
	$("#btn").click(function(){
		$("tr:last").before("<tr><td> Poreklo :</td><td><input  type='text'/></td></tr>");
	});	
	});
</script>

</head>
<body>

<% List<Autor> autori = (List<Autor>) session.getAttribute("autori"); %>

<h2>Dodavanje autora</h2>
<form action  = "DodajAutoraServlet" method = "POST" name = "loginForma"  onSubmit = "return provera()">
<table>
<tr><td>Ime</td><td><input name = "ime" onfocus = "usao('imeSpan')" onblur ="izasao(this, 'imeSpan')"/><span style = "color:red; visibility: hidden" id = "imeSpan">Popunite polje</span>
</td></tr>
<tr><td>Prezime</td><td><input name = "prezime" onfocus = "usao('prezimeSpan')" onblur = "izasao(this, 'prezimeSpan')"/><span style = "color:red; visibility: hidden" id = "prezimeSpan">Popunite polje</span></td></tr>
<tr><td><input type = "submit" value = "Submit"/></td><td><input type = "button" id = "btn" value="Dodaj poreklo"/></td></tr>
</table>
</form>


<h2>Dodavanje knjiga</h2>
<form action  = "DodajKnjiguServlet" method = "POST">
<table>
<tr><td>Naslov</td><td><input name = "naslov"/></td>
<tr>
<td>Autor</td>
<td><select name = "autori">
<% for (Autor a : autori) {%>
	<option value = "<%=a.getId()%>"><%= a.getIme( )+ " " +  a.getPrezime() %></option>
<%} %>
</select>
</td>
</tr>
<tr><td><input type = "submit" value = "Submit"/></td></tr>
</table>
</form>



</body>
</html>