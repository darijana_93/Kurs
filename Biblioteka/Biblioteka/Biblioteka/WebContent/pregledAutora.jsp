<%@ page import = "biblioteka.model.Autor"%>
<%@ page import = "java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<% List<Autor> autori = (List<Autor>) session.getAttribute("autori"); %>
<h1>Autori</h1>
<table border ="1">
<tr><th>Ime</th><th>Prezime</th><th>Knjige</th></tr>
<% for (Autor a : autori) {%>
	<tr><td><%=a.getIme() %></td><td><%=a.getPrezime()%></td><td><a href="ListaKnjigaPoAutoruServlet?id_autora=<%=a.getId()%>"> Spisak knjiga</a></td></tr>
<%} %>

</table>

</body>
</html>